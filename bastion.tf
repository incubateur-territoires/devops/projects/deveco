resource "scaleway_vpc_private_network" "bastion" {
  provider = scaleway.scaleway_project
  name     = "bastion"
}
resource "scaleway_vpc_public_gateway_ip" "bastion" {
  provider = scaleway.scaleway_project
}
resource "scaleway_vpc_public_gateway" "bastion" {
  name            = "bastion"
  provider        = scaleway.scaleway_project
  ip_id           = scaleway_vpc_public_gateway_ip.bastion.id
  type            = "VPC-GW-S"
  bastion_enabled = true
}
resource "scaleway_vpc_gateway_network" "bastion" {
  provider           = scaleway.scaleway_project
  private_network_id = scaleway_vpc_private_network.bastion.id
  gateway_id         = scaleway_vpc_public_gateway.bastion.id
  ipam_config {
    push_default_route = true
  }
}

data "gitlab_user_sshkeys" "devs" {
  for_each = var.devs_gitlab_usernames
  username = each.value
}
locals {
  all_dev_keys_ids = flatten([
    for user in var.devs_gitlab_usernames : [for key in data.gitlab_user_sshkeys.devs[user].keys : key.key_id]
    ]
  )
  all_dev_keys_data = flatten([
    for user in var.devs_gitlab_usernames : [
      for key in data.gitlab_user_sshkeys.devs[user].keys : {
        key   = key.key,
        user  = user,
        title = key.title,
      }
    ]
  ])
  all_dev_keys = zipmap(local.all_dev_keys_ids, local.all_dev_keys_data)
}
resource "scaleway_account_ssh_key" "devs" {
  for_each   = local.all_dev_keys
  provider   = scaleway.scaleway_project
  name       = "GitLab key for ${each.value.user} (${each.value.title}) (managed by terraform)"
  public_key = each.value.key
}
