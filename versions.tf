terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.11.0"
    }
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.40.0"
    }
    grafana = {
      source  = "grafana/grafana"
      version = "~> 1.27.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.3.2"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.9.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.13.0"
    }
  }
  required_version = "~> 1.8.1"
}
