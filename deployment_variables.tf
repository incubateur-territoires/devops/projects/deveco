# Scripted import of existing deployment variables
# Could be refactored to be split across the environment scopes
# Uses the random_password hack to store secrets in the state and avoid having to pass lots of secrets as variables

resource "gitlab_project_variable" "development_helm_backend_env_var_sandbox_login" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "development"

  key   = "HELM_BACKEND_ENV_VAR_SANDBOX_LOGIN"
  value = "\"sandbox\""
}
resource "random_password" "development_helm_backend_env_var_jwt_key" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "development_helm_backend_env_var_jwt_key" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "development"

  key   = "HELM_BACKEND_ENV_VAR_JWT_KEY"
  value = random_password.development_helm_backend_env_var_jwt_key.result
}
resource "random_password" "shared_helm_backend_env_var_import_token" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "shared_helm_backend_env_var_import_token" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "*"

  key   = "HELM_BACKEND_ENV_VAR_IMPORT_TOKEN"
  value = random_password.shared_helm_backend_env_var_import_token.result
}
resource "random_password" "development_helm_backend_env_var_insee_update_token" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "development_helm_backend_env_var_insee_update_token" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "development"

  key   = "HELM_BACKEND_ENV_VAR_INSEE_UPDATE_TOKEN"
  value = random_password.development_helm_backend_env_var_insee_update_token.result
}
resource "gitlab_project_variable" "shared_helm_backend_env_var_insee_api" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "*"

  key   = "HELM_BACKEND_ENV_VAR_INSEE_API"
  value = "https://api.insee.fr/entreprises/sirene/V3.11"
}
resource "random_password" "shared_helm_backend_env_var_insee_access_token" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "random_password" "development_helm_backend_env_var_nouveautes_update_token" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "development_helm_backend_env_var_nouveautes_update_token" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "development"

  key   = "HELM_BACKEND_ENV_VAR_NOUVEAUTES_UPDATE_TOKEN"
  value = random_password.development_helm_backend_env_var_nouveautes_update_token.result
}
resource "random_password" "production_helm_backend_env_var_smtp_port" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "production_helm_backend_env_var_smtp_port" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_SMTP_PORT"
  value = random_password.production_helm_backend_env_var_smtp_port.result
}
resource "random_password" "production_helm_backend_env_var_smtp_pass" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "production_helm_backend_env_var_smtp_pass" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_SMTP_PASS"
  value = random_password.production_helm_backend_env_var_smtp_pass.result
}
resource "random_password" "production_helm_backend_env_var_smtp_user" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "production_helm_backend_env_var_smtp_user" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_SMTP_USER"
  value = random_password.production_helm_backend_env_var_smtp_user.result
}
resource "random_password" "production_helm_backend_env_var_smtp_host" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "production_helm_backend_env_var_smtp_host" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_SMTP_HOST"
  value = random_password.production_helm_backend_env_var_smtp_host.result
}
resource "random_password" "production_helm_backend_env_var_smtp_from" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "production_helm_backend_env_var_smtp_from" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_SMTP_FROM"
  value = random_password.production_helm_backend_env_var_smtp_from.result
}
resource "gitlab_project_variable" "production_helm_backend_env_var_app_url" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_APP_URL"
  value = "https://${var.prod_base_domain}"
}
resource "random_password" "production_helm_backend_env_var_jwt_key" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "production_helm_backend_env_var_jwt_key" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_JWT_KEY"
  value = random_password.production_helm_backend_env_var_jwt_key.result
}
resource "random_password" "production_helm_backend_env_var_smtp_replyto" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "production_helm_backend_env_var_smtp_replyto" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_SMTP_REPLYTO"
  value = random_password.production_helm_backend_env_var_smtp_replyto.result
}
resource "gitlab_project_variable" "production_helm_backend_env_var_track_logs_activity" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_TRACK_LOGS_ACTIVITY"
  value = "true"
}
resource "gitlab_project_variable" "production_helm_backend_env_var_rappels_reminder_schedule" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_RAPPELS_REMINDER_SCHEDULE"
  value = "0 22 * * *"
}
resource "random_password" "production_helm_backend_env_var_insee_update_token" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "production_helm_backend_env_var_insee_update_token" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_INSEE_UPDATE_TOKEN"
  value = random_password.production_helm_backend_env_var_insee_update_token.result
}
resource "random_password" "production_helm_backend_env_var_nouveautes_update_token" {
  length = 128
  lifecycle {
    ignore_changes = all
  }
}
resource "gitlab_project_variable" "production_helm_backend_env_var_nouveautes_update_token" {
  project           = local.gitlab_project_ids.deveco
  environment_scope = "production"

  key   = "HELM_BACKEND_ENV_VAR_NOUVEAUTES_UPDATE_TOKEN"
  value = random_password.production_helm_backend_env_var_nouveautes_update_token.result
}
