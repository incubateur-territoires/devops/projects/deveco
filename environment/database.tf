locals {
  db_backups = contains(["development", "production"], var.gitlab_environment_scope)
  db_config = (
    var.gitlab_environment_scope == "development" ? {
      host     = scaleway_rdb_instance.database[0].load_balancer[0].ip
      port     = scaleway_rdb_instance.database[0].load_balancer[0].port
      user     = scaleway_rdb_user.app[0].name
      password = urlencode(scaleway_rdb_user.app[0].password)
      dbname   = scaleway_rdb_database.app[0].name
    }
    : var.gitlab_environment_scope == "production" ? {
      host     = scaleway_rdb_instance.database[0].load_balancer[0].ip
      port     = scaleway_rdb_instance.database[0].load_balancer[0].port
      user     = scaleway_rdb_user.app[0].name
      password = urlencode(scaleway_rdb_user.app[0].password)
      dbname   = scaleway_rdb_database.app[0].name
    }
    : {
      host     = "" # determined at deploy time for reviews
      port     = "5432"
      user     = "deveco"
      password = random_password.deveco_reviews_deveco_password[0].result
      dbname   = "deveco"
    }
  )
}
