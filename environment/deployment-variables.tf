moved {
  from = module.configure-repository-for-deployment
  to   = module.configure_repository_for_deployment
}

locals {
  db_config_secret_name = "${var.project_slug}-db-config"
}
resource "random_password" "deveco_reviews_postgres_password" {
  count   = var.gitlab_environment_scope == "*" ? 1 : 0
  special = false
  length  = 64
}
resource "random_password" "deveco_reviews_deveco_password" {
  count   = var.gitlab_environment_scope == "*" ? 1 : 0
  special = false
  length  = 64
}
moved {
  from = kubernetes_secret_v1.deveco_reviews_db_config[0]
  to   = kubernetes_secret_v1.db_config
}
resource "kubernetes_secret_v1" "db_config" {
  metadata {
    namespace = module.namespace.namespace
    name      = local.db_config_secret_name
  }
  data = local.db_config
}
# keep legacy secret name while there are still deployments using it
resource "kubernetes_secret_v1" "legacy_reviews_db_secret" {
  count = var.gitlab_environment_scope == "*" ? 1 : 0
  metadata {
    namespace = module.namespace.namespace
    name      = "reviews-postgresql-auth"
  }
  data = local.db_config
}
resource "gitlab_project_variable" "deveco_helm_values" {
  count             = 1
  project           = var.gitlab_project_ids.deveco
  environment_scope = var.gitlab_environment_scope
  variable_type     = "file"

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
  backend:
    resources:
      limits:
        memory: 2Gi
      requests:
        cpu: 10m
    envVars:
      ${var.gitlab_environment_scope == "*" ? "" : "POSTGRES_CONNECTION_OPTIONS: '{\"ssl\": { \"rejectUnauthorized\": false }}'"}
      POSTGRES_HOST:
        secretKeyRef:
          name: ${local.db_config_secret_name}
          key: host
      POSTGRES_PORT:
        secretKeyRef:
          name: ${local.db_config_secret_name}
          key: port
      POSTGRES_DB:
        secretKeyRef:
          name: ${local.db_config_secret_name}
          key: dbname
      POSTGRES_USERNAME:
        secretKeyRef:
          name: ${local.db_config_secret_name}
          key: user
      POSTGRES_USER:
        secretKeyRef:
          name: ${local.db_config_secret_name}
          key: user
      POSTGRES_PASSWORD:
        secretKeyRef:
          name: ${local.db_config_secret_name}
          key: password

    ingress:
      enabled: true
      path: /api/
      annotations:
        kubernetes.io/ingress.class: haproxy
        haproxy.org/path-rewrite: /api/(.*) /\1
        haproxy.org/backend-config-snippet: |
          timeout server 120s

    service:
      targetPort: 3000

    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
      prometheus.io/scrape: "true"
      prometheus.io/port: "9102"

    probes:
      liveness:
        ${var.gitlab_environment_scope == "*" ? "tcpSocket: {port: 3000}" : "path: /health"}
        initialDelaySeconds: 0
      readiness:
        ${var.gitlab_environment_scope == "*" ? "tcpSocket: {port: 3000}" : "path: /health"}
        initialDelaySeconds: 0

    monitoring:
      exporter:
        enabled: true
      dashboard:
        deploy: true

    serviceAccount:
      create: false

  frontend:
    resources:
      limits:
        memory: 512Mi
      requests:
        cpu: 10m

    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: haproxy

    service:
      targetPort: 80

    monitoring:
      exporter:
        enabled: true
      dashboard:
        deploy: true

    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}

    probes:
      liveness:
        initialDelaySeconds: 0
      readiness:
        initialDelaySeconds: 0

    serviceAccount:
      create: false

  ${local.review_postgresql_helm_values}
  EOT
}
locals {
  review_postgresql_helm_values = var.gitlab_environment_scope != "*" ? "" : <<-EOT
  postgresql:
    strategy:
      type: Recreate
    enabled: true
    envVars:
      POSTGRES_DB:
        secretKeyRef:
          name: ${local.db_config_secret_name}
          key: dbname
      POSTGRES_USER:
        secretKeyRef:
          name: ${local.db_config_secret_name}
          key: user
      POSTGRES_PASSWORD:
        secretKeyRef:
          name: ${local.db_config_secret_name}
          key: password
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
    resources:
      limits:
        memory: 512Mi
      requests:
        cpu: 100m
  EOT
}

resource "gitlab_project_variable" "website_helm_values" {
  project           = var.gitlab_project_ids.website
  environment_scope = var.gitlab_environment_scope
  variable_type     = "file"

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
    resources:
      limits:
        memory: 512Mi
      requests:
        cpu: 10m

    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: haproxy
      hosts:
        - host: $HELM_INGRESS_HOST
          tls: true
          paths:
            - path: /accessibilite
            - path: /cgu
            - path: /documentation
            - path: /demonstration
            - path: /webinaires
            - path: /faq
            - path: /mentions-legales
            - path: /newsletter
            - path: /nouveautes
            - path: /politique-de-confidentialite
            - path: /statistiques
            - path: /votre-avis
            - path: /
              pathType: Exact
            - path: /dsfr
            - path: /images
            - path: /deveco.css
              pathType: Exact

    service:
      targetPort: 4321

    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}

    probes:
      liveness:
        initialDelaySeconds: 0
      startup:
        initialDelaySeconds: 0
  EOT
}
