locals {
  environment_slug = var.gitlab_environment_scope == "*" ? "review" : var.gitlab_environment_scope
}

module "kubeconfig_augustin" {
  count   = var.gitlab_environment_scope == "*" ? 0 : 1
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.1.0"

  filename               = "deveco-augustin-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "augustin"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}

module "kubeconfig_augustin_ragon" {
  count   = var.gitlab_environment_scope == "*" ? 1 : 0
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.3.1"

  filename               = "deveco-augustin-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "augustin-ragon"
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}

module "kubeconfig_adrien_risser" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.3.1"

  filename               = "deveco-adrien-risser-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "adrien-risser"
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
