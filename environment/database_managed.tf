locals {
  deploy_managed_db = contains(["development", "production"], var.gitlab_environment_scope) ? true : false
}

resource "scaleway_rdb_instance" "database" {
  count = local.deploy_managed_db ? 1 : 0
  name  = "${var.project_slug}-${var.gitlab_environment_scope}"

  engine            = "PostgreSQL-15"
  node_type         = "DB-POP2-8C-32G"
  is_ha_cluster     = var.gitlab_environment_scope == "production"
  volume_type       = "sbs_5k"
  volume_size_in_gb = var.database_disk_size

  disable_backup            = false
  backup_schedule_frequency = 24
  backup_schedule_retention = 30

  settings = {
    "rdb.enable_pgaudit"              = true
    "pgaudit.log"                     = "ALL"
    "effective_cache_size"            = 20000
    "maintenance_work_mem"            = 800
    "max_connections"                 = 800
    "max_parallel_workers"            = 6
    "max_parallel_workers_per_gather" = 3
    "work_mem"                        = 24
  }

  private_network {
    pn_id       = var.bastion_private_network_id
    enable_ipam = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "scaleway_rdb_database" "app" {
  count       = local.deploy_managed_db ? 1 : 0
  instance_id = scaleway_rdb_instance.database[0].id
  name        = var.project_slug

  lifecycle {
    prevent_destroy = true
  }
}

resource "random_password" "database_app_password" {
  count  = local.deploy_managed_db ? 1 : 0
  length = 64
}
resource "scaleway_rdb_user" "app" {
  count       = local.deploy_managed_db ? 1 : 0
  instance_id = scaleway_rdb_instance.database[0].id
  name        = var.project_slug
  password    = random_password.database_app_password[0].result
}
resource "scaleway_rdb_privilege" "app" {
  count         = local.deploy_managed_db ? 1 : 0
  instance_id   = scaleway_rdb_instance.database[0].id
  user_name     = scaleway_rdb_user.app[0].name
  database_name = scaleway_rdb_database.app[0].name
  permission    = "all"
}

resource "scaleway_rdb_acl" "app" {
  count       = local.deploy_managed_db ? 1 : 0
  instance_id = scaleway_rdb_instance.database[0].id
  acl_rules {
    description = "K8S public gateway IP"
    ip          = "${var.k8s_public_gw_ip}/32"
  }
}

resource "kubernetes_config_map_v1" "tunnel_command" {
  count = local.deploy_managed_db ? 1 : 0
  metadata {
    namespace = module.namespace.namespace
    name      = "doc-tunnel-command"
  }
  data = {
    command = "ssh -N -4 -p ${var.bastion_port} -L <local port>:${scaleway_rdb_instance.database[0].private_network[0].ip}:${scaleway_rdb_instance.database[0].private_network[0].port} bastion@${var.bastion_ip} # for ${var.gitlab_environment_scope}"
  }
}
