variable "base_domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}
locals {
  environment_name = var.gitlab_environment_scope == "*" ? "reviews" : var.gitlab_environment_scope
}

variable "namespace" {
  type = string
}

variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}

variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}

# tflint-ignore: terraform_unused_declarations
variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}

variable "monitoring_org_id" {
  type = string
}

variable "gitlab_project_ids" {
  type = object({
    deveco  = number
    website = number
  })
}

variable "k8s_public_gw_ip" {
  type    = string
  default = null
}

variable "bastion_private_network_id" {
  type = string
}
variable "bastion_port" {
  type    = number
  default = null
}
variable "bastion_ip" {
  type    = string
  default = null
}

variable "database_disk_size" {
  type = number
}
