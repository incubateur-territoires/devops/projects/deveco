locals {
  repository = "registry.gitlab.com/incubateur-territoires/startups/deveco/deveco-sirene/main"
  imports = var.gitlab_environment_scope == "*" ? {} : {
    initial = contains(["development"], var.gitlab_environment_scope) ? {
      tag    = "4adf4e1e85bd4dd89882934ed2b225460197a53e"
      script = "scripts/import_initial.sh"
      } : {
      tag    = "92f381dbc9ce8b3a748256563fc66e76118a6f2f"
      script = "scripts/import_initial.sh"
    },
    sirene = contains(["development"], var.gitlab_environment_scope) ? {
      tag    = "40d6e4996fdd103333bd4fa66ece345f49b005eb"
      script = "./import_sirene.sh"
      } : {
      tag    = "92f381dbc9ce8b3a748256563fc66e76118a6f2f"
      script = "scripts/import_sirene.sh"
    }
  }
}
resource "kubernetes_cron_job_v1" "import_sirene" {
  for_each = local.imports
  metadata {
    name      = "import-${each.key}"
    namespace = module.namespace.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = "0 0 1 * *"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10
    suspend                       = true

    job_template {
      metadata {
        annotations = {}
        labels      = {}
      }

      spec {
        backoff_limit = 0

        template {
          metadata {
            annotations = {}
            labels      = {}
          }
          spec {
            container {
              name    = "import-${each.key}"
              image   = "${local.repository}:${each.value.tag}"
              command = [each.value.script]
              resources {
                limits = {
                  cpu    = "1000m"
                  memory = "512Mi"
                }
                requests = {
                  cpu    = "512m"
                  memory = "256Mi"
                }
              }
              env {
                name  = "LOG_FILE"
                value = "/dev/stdout"
              }
              env {
                name  = "POSTGRES_HOST"
                value = local.db_config.host
              }
              env {
                name  = "POSTGRES_USERNAME"
                value = local.db_config.user
              }
              env {
                name  = "POSTGRES_PASSWORD"
                value = local.db_config.password
              }
              env {
                name  = "POSTGRES_PORT"
                value = local.db_config.port
              }
              env {
                name  = "POSTGRES_DB"
                value = local.db_config.dbname
              }
            }
          }
        }
      }
    }
  }
}
