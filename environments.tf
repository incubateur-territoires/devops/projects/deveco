locals {
  db_memory_gi       = 20 # memory allocated to the db container
  db_other_memory_gi = 3  # memory allocated to the sidecars of each db container
  other_memory_gi    = 2  # memory left for other containers in the namespace
  dev_db_instances   = 2
  prod_db_instances  = 3
  dev_memory_gi      = (local.db_memory_gi + local.db_other_memory_gi) * local.dev_db_instances + local.other_memory_gi
  prod_memory_gi     = (local.db_memory_gi + local.db_other_memory_gi) * local.prod_db_instances + local.other_memory_gi
}
module "reviews" {
  source                   = "./environment"
  gitlab_environment_scope = "*"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.dev_base_domain
  namespace                = "${var.project_slug}-reviews"
  project_slug             = var.project_slug
  project_name             = var.project_name
  gitlab_project_ids       = local.gitlab_project_ids

  namespace_quota_max_cpu    = 4
  namespace_quota_max_memory = "16Gi"
  database_disk_size         = 500

  monitoring_org_id = random_string.development_secret_org_id.result

  scaleway_project_config    = var.scaleway_project_config
  bastion_private_network_id = scaleway_vpc_private_network.bastion.id
  providers = {
    scaleway = scaleway.scaleway_project
  }
}

module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.dev_base_domain
  namespace                = "${var.project_slug}-development"
  project_slug             = var.project_slug
  project_name             = var.project_name
  gitlab_project_ids       = local.gitlab_project_ids

  namespace_quota_max_cpu    = 18
  namespace_quota_max_memory = "${local.dev_memory_gi}Gi"
  database_disk_size         = 500

  monitoring_org_id = random_string.development_secret_org_id.result

  scaleway_project_config    = var.scaleway_project_config
  bastion_private_network_id = scaleway_vpc_private_network.bastion.id
  bastion_ip                 = scaleway_vpc_public_gateway_ip.bastion.address
  bastion_port               = scaleway_vpc_public_gateway.bastion.bastion_port

  k8s_public_gw_ip = var.k8s_development_public_gw_ip
  providers = {
    scaleway = scaleway.scaleway_project
  }
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base_domain              = var.prod_base_domain
  namespace                = var.project_slug
  project_slug             = var.project_slug
  project_name             = var.project_name
  gitlab_project_ids       = local.gitlab_project_ids

  namespace_quota_max_cpu    = 18
  namespace_quota_max_memory = "${local.prod_memory_gi}Gi"
  database_disk_size         = 700

  monitoring_org_id = random_string.production_secret_org_id.result

  scaleway_project_config    = var.scaleway_project_config
  bastion_private_network_id = scaleway_vpc_private_network.bastion.id
  bastion_ip                 = scaleway_vpc_public_gateway_ip.bastion.address
  bastion_port               = scaleway_vpc_public_gateway.bastion.bastion_port

  k8s_public_gw_ip = var.k8s_production_public_gw_ip
  providers = {
    scaleway = scaleway.scaleway_project
  }
}
