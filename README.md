#

## Add git hooks

```shell
git config [--local] core.hooksPath .hooks
```

## Outputting SSH commands from null_resources

```bash
tofu state pull | jq '.resources[] | select(.type == "null_resource" and .name == "tunnel_command") | .instances[].attributes.triggers[]' -r
```
