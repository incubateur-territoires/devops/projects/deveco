module "kubeconfig_augustin" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.3.1"

  filename               = "deveco-augustin-${var.project_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "augustin"
  project_name           = "Metabase"
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
