variable "hostname" {
  type = string
}

variable "project_slug" {
  type = string
}

# tflint-ignore: terraform_unused_declarations
variable "scaleway_organization_id" {
  type = string
}
variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}
# tflint-ignore: terraform_unused_declarations
variable "backup_offset_minutes" {
  type        = number
  description = "Minute at which the backup should take place. Used to prevent all backups from running at the same time."
}

variable "monitoring_org_id" {
  type      = string
  sensitive = true
}
