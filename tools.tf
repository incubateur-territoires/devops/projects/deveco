resource "scaleway_object_bucket" "legacy_zammad_db_backups" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-zammad-db-backups"
}
moved {
  from = module.zammad.scaleway_object_bucket.db_backups
  to   = scaleway_object_bucket.legacy_zammad_db_backups
}

module "metabase" {
  source     = "./tools/metabase"
  kubeconfig = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  hostname   = "metabase.${var.prod_base_domain}"

  project_slug = "${var.project_slug}-metabase"

  backup_offset_minutes    = 50
  scaleway_organization_id = var.scaleway_organization_id
  scaleway_project_config  = var.scaleway_project_config
  monitoring_org_id        = random_string.production_secret_org_id.result
  providers = {
    scaleway = scaleway.scaleway_project
  }
}
